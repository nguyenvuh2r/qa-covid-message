<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Arr;

class MessengerController extends Controller
{
    /**
     * The verification token for Facebook
     *
     * @var string
     */
    protected $token;

    public function __construct()
    {
        $this->token = 'vudeptrai@@@';
    }

    /**
     * Verify the token from Messenger. This helps verify your bot.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function verify_token(Request $request)
    {
        $mode  = $request->get('hub_mode');
        $token = $request->get('hub_verify_token');

        if ($mode == "subscribe" && $token == $this->token) {
            return response($request->get('hub_challenge'));
        }

        return response("Invalid token!", 400);
    }

    /**
     * Handle the query sent to the bot.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response|\Laravel\Lumen\Http\ResponseFactory
     */
    public function handle_query(Request $request)
    {
        //Log::info($request);
        $entry = $request->get('entry');

        $sender  = Arr::get($entry, '0.messaging.0.sender.id');
        $message = mb_strtolower(Arr::get($entry, '0.messaging.0.message.text'), 'UTF-8');

        if($message == null || $message == '') {
            return response('', 200);
        }

        if(strpos($message, 'covid') !== false) {
            $info = trim(str_replace('covid ', '', $message));

            if($info !== null) {
                $res_msg = '';
                if ($info === 'việt nam') {
                    $api_result = $this->fetchAPI("https://api-kent.netlify.app/.netlify/functions/api/vn/daily/covid");

                    $res_msg = "Thông tin covid Việt Nam ngày " . $api_result['lastUpdated'] . " :\n";
                    $res_msg .= "- Ca nhiễm hôm nay: " . $api_result['data']['0']['new_cases'] . "\n";
                    $res_msg .= "- Ca chết hôm nay: " . $api_result['data']['0']['new_deaths']  . "\n";
                    $res_msg .= "- Tổng số ca nhiễm: " . $api_result['data']['0']['total_cases']  . "\n";
                    $res_msg .= "- Tổng số ca chết: " . $api_result['data']['0']['total_deaths']  . "\n";
                }
                else {
                    $api_result = $this->fetchAPI("https://api-kent.netlify.app/.netlify/functions/api/vn/");
                    $province_info = $this->findProvinceInfo($api_result, $info);

                    if($province_info !== null) {
                        $res_msg = "Thông tin covid tỉnh " . $province_info['name'] .":\n";
                        $res_msg .= "- Số ca hồi phục: " .  $province_info['recovered']. "\n";
                        $res_msg .= "- Tổng số ca nhiễm: " . $province_info['cases'] . "\n";
                        $res_msg .= "- Tổng số ca chết: " . $province_info['deaths'] ."\n";
                        $res_msg .= "Cập nhật lần cuối: " . $api_result['lastUpdated'] . "\n";
                    }
                    else {
                        $res_msg = "Tỉnh này không tồn tại";
                    }
                }

                if($res_msg != '') {
                    $this->dispatchResponse($sender, $res_msg);
                } else {
                    $this->dispatchResponse($sender, "Có lỗi xảy ra khi nhận thông tin, vui lòng thử lại sau.");
                }
            }
        }
        else {
            $this->dispatchResponse($sender, $this->messageIntroduce());
        }

        return response('', 200);
    }

    private function send_api($recipient_id, $message_text) {
        $post_message_url = 'https://graph.facebook.com/v9.0/me/messages?access_token=' . App::environment('FB_MESSENGER_PAGE_ACCESS_TOKEN');
        Http::withHeaders([
                'Content-Type' => 'application/json'
            ])
            ->post($post_message_url, [
            'recipient' => [
                'id' => $recipient_id
            ],
            'message' => $message_text
        ]);
    }

    /**
     * Get province info from API.
     *
     * @param  string  $name
     * @return array
     */
    protected function fetchAPI($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        
        $arr_result = json_decode($result, true);
        return $arr_result;
    }

    /**
     * Post a message to the Facebook messenger API.
     *
     * @param  integer $id
     * @param  string  $response
     * @return bool
     */
    protected function dispatchResponse($id, $response)
    {
        $access_token = "EAADMw4QF2ioBAEVVc5rBv6m2hXnf4xeVGCjjfABd6ZAKbKDlPuMjq4tZBS8AZCNTqZAuMPRdZBmt0ZCrTRYg0WfuOyOvZAM4ZCrzlmLbkcHDM9CuOZBLXwKNDRk7ukJIgYdyTKrvcY0o6hlV08MsOUwsKXOkJZArl2kymyXsHtJNOPkJNDIJQkHZCBG";
        $url = "https://graph.facebook.com/v11.0/me/messages?access_token={$access_token}";

        $data = json_encode([
            'recipient' => ['id' => $id],
            'message'   => ['text' => $response]
        ]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    protected function findProvinceInfo($arr, $provinceName)
    {
        foreach($arr['detail'] as $detail) {
            if(mb_strtolower($detail['name'], 'UTF-8') === $provinceName)
            {
                return $detail;
            }
        }
        return null;
    }

    protected function messageIntroduce() {
        $intro = "Nhận thông tin Covid nhanh nhất bằng cách nhập thông tin muốn xem:\n";
        $intro .= "- Covid Việt Nam\n";
        $intro .= "- Covid Thế giới (Đang cập nhật)\n";
        $intro .= "- Covid + Tên tỉnh thành\n";
        $intro .= "Xem chi tiết hơn tại website.";
 
        return $intro;
    }
}
